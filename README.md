# Final project on [spatial database](https://en.wikipedia.org/wiki/Spatial_database)
## Name: Expecting best possible school location using different attributes
## Instructor: [S.M. Farabi Mahmud](http://cse.uiu.ac.bd/profiles/mahmud-s-m-farabi/)
### Trimester: Summer'18
### Section: A
***
Description: It is a research based project.
***
###Referances:
1. [Class-based Conditional MaxRS Query in Spatial Data Streams](https://www.researchgate.net/publication/317354414_Class-based_Conditional_MaxRS_Query_in_Spatial_Data_Streams)