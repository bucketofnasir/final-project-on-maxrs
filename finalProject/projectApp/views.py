from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.views.generic import ListView, DetailView
from .utils import rect
from .weight_calculation import WeightCalculate
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from .models import SpatialData, Junction, AreaInfo, Documentation
from .forms import CreateObject, SignUpForm, CreateJunction
from .distinct_data import data as manifest
from . import priority
# Create your views here.

class getHome(View):
    def get(self, request):
        return render(request, 'home.html')

class VisualizeForm(View):
    def get(self, request):
        return render(request,'visual_form.html', manifest.distinct(self))

class VisualizeData(View):
    def get(self, request):
        data=SpatialData.objects.exclude(longitude=0).exclude(longitude=-1).exclude(lattitude=0).exclude(lattitude=-1)
        junction=Junction.objects.all()
        if request.GET.get('type'):
            data = data.filter(
                Q(level=request.GET.get('type'))
            )

        if request.GET.get('area'):
            data=data.filter(
                Q(thana=request.GET.get('area'))
            )
        if request.GET.get('management'):
            data=data.filter(
                Q(management=request.GET.get('management'))
            )

        if request.GET.get('area_type'):
            data=data.filter(
                Q(area_type=request.GET.get('area_type'))
            )

        if request.GET.get('geography'):
            data=data.filter(
                Q(geography=request.GET.get('geography'))
            )
        for d in data:
            d.lattitude=float(d.lattitude)
            d.longitude=float(d.longitude)

        blx, bly, brx, bry, tlx, tly, trx, tpry = [], [], [], [], [], [], [], []
        a = 0.00990  # Width of rectangle
        b = 0.0027  # Distance of spatial point from rectangular border
        c = a * 2  # Length of rectangle. 2x of width
        no_object_in_rect = []  # Number of object in each rectangle
        '''Creating rectangle based on object'''
        for d in data:
            if d.lattitude > 0 and d.longitude > 0:
                flag = 0
                for j in range(0, len(blx)):
                    if rect.is_in_rectangle(blx[j], bly[j], brx[j], bry[j], trx[j], tpry[j], tlx[j], tly[j],
                                            d.longitude, d.lattitude):
                        flag = 1
                        break
                if flag == 0:
                    blx.append(d.longitude - b)
                    bly.append(d.lattitude - b)
                    brx.append(d.longitude + c - b)
                    bry.append(d.lattitude - b)
                    tlx.append(d.longitude - b)
                    tly.append(d.lattitude + a - b)
                    trx.append(d.longitude + c - b)
                    tpry.append(d.lattitude + a - b)
                    
        

        '''******************************************
        Creating rectangle based on junction
        ******************************************
        for j in junction:
            blx.append(float(j.longitude) - b)
            bly.append(float(j.lattitude) - b)
            brx.append(float(j.longitude) + c - b)
            bry.append(float(j.lattitude) - b)
            tlx.append(float(j.longitude) - b)
            tly.append(float(j.lattitude) + a - b)
            trx.append(float(j.longitude) + c - b)
            tpry.append(float(j.lattitude) + a - b)

        ******************************************
        Find the number of object in each rectangle
        *******************************************'''
        for i in range(0, len(blx)):
            count = 0
            for d in data:
                if blx[i] <= d.longitude and bly[i] <= d.lattitude and brx[i] >= d.longitude and bry[i] <= d.lattitude and tlx[i] <= d.longitude and tly[i] >= d.lattitude and trx[i] >= d.longitude and tpry[i] >= d.lattitude:
                    count = count + 1
                else:
                    pass
            no_object_in_rect.append(count)

        '''**************************************************
        Calculating the weights of each object in a rectangle
        **************************************************'''
        weight = []
        for i in range(0, len(blx)):
            wcount = 0
            for d in data:
                if blx[i] <= d.longitude and bly[i] <= d.lattitude and brx[i] >= d.longitude and bry[i] <= d.lattitude and tlx[i] <= d.longitude and tly[i] >= d.lattitude and trx[i] >= d.longitude and tpry[i] >= d.lattitude:
                    list= [no_object_in_rect[i], float(d.high_income), float(d.mid_income), float(d.low_income),
                           priority.geography(d.geography)]
                    if d.gender_type == "CO-EDUCATION JOINT" or d.gender_type == "C0-EDUCATION SEPARATE":
                        if d.level == 'SECONDARY':
                            list=list+priority.Secondary()
                        else:
                            list=list+priority.primary()
                    wcount = wcount + WeightCalculate.calculate(list)
                else:
                    pass
            weight.append(wcount)

        try:
            x = min(weight)
            mxrs = weight.index(x)
        except:
            mxrs = -1
        print(weight)
        context = {
            "data":data, "blx": blx, "bly": bly, "brx": brx, "bry": bry, "tlx": tlx, "tly": tly, "trx": trx, "tpry": tpry, "mxrs_index": mxrs
        }
        context.update(manifest.distinct(self))
        return render(request, 'visual_from_existing_data.html', context)




class getIndex(View):
    template = "index.html"

    def get(self, request):
        return render(request, self.template)

    def post(self, request):
        type = request.POST.get('type')
        area = request.POST.get('area')
        fs = request.FILES['csv']
        data = fs.read().decode("ANSI")
        lines = data.split("\n")
        x = []
        lat = []
        lon = []
        high, medium, low = [], [], []
        for line in lines:
            fields = line.split(",")
            x.append(fields)

        if area:
            for line in x:
                if len(line) > 1:
                    if type == line[3]:
                        if area == line[7]:
                            if line[17] == '\r' or line[17] == '' or line[16] == 'Longitude' or float(line[17]) == -1 or float(line[16]) == -1:
                                pass
                            else:
                                lat.append(float(line[17]))
                                lon.append(float(line[16]))
                                high.append(float(line[18]))
                                medium.append(float(line[19]))
                                low.append(float(line[20]))
        else:
            for line in x:
                if len(line) > 1:
                    if type == line[3]:
                        if line[17] == '\r' or line[17] == '' or line[16] == 'Longitude':
                            pass
                        else:
                            lat.append(float(line[17]))
                            lon.append(float(line[16]))
                            high.append(float(line[18]))
                            medium.append(float(line[19]))
                            low.append(float(line[20]))

        blx, bly, brx, bry, tlx, tly, trx, tpry = [], [], [], [], [], [], [], []
        a = 0.00970  # Width of rectangle
        b = 0.00570  # Distance of spatial point from rectangular border
        c = a * 1.8  # Length of rectangle. 2x of width
        no_object_in_rect = []  # Number of object in each rectangle
        for i in range(0, len(lat)):
            if float(lat[i]) > 0 and float(lon[i]) > 0:
                flag = 0
                for j in range(0, len(blx)):
                    if rect.is_in_rectangle(blx[j], bly[j], brx[j], bry[j], trx[j], tpry[j], tlx[j], tly[j],
                                            float(lon[i]), float(lat[i])):
                        flag = 1
                        break
                if flag == 0:
                    blx.append(lon[i] - b)
                    bly.append(lat[i] - b)
                    brx.append(lon[i] + c - b)
                    bry.append(lat[i] - b)
                    tlx.append(lon[i] - b)
                    tly.append(lat[i] + a - b)
                    trx.append(lon[i] + c - b)
                    tpry.append(lat[i] + a - b)

        '''******************************************
        Find the number of object in each rectangle
        *******************************************'''
        for i in range(0, len(blx)):
            count = 0
            for j in range(0, len(lon)):
                if blx[i] <= lon[j] and bly[i] <= lat[j] and brx[i] >= lon[j] and bry[i] <= lat[j] and tlx[i] <= lon[
                    j] and tly[i] >= lat[j] and trx[i] >= lon[j] and tpry[i] >= lat[j]:
                    count = count + 1
                else:
                    pass
            no_object_in_rect.append(count)

        print(no_object_in_rect)

        '''**************************************************
        Calculating the weights of each object in a rectangle
        **************************************************'''
        weight = []
        for i in range(0, len(blx)):
            wcount = 0
            for j in range(0, len(lon)):
                if blx[i] <= lon[j] and bly[i] <= lat[j] and brx[i] >= lon[j] and bry[i] <= lat[j] and tlx[i] <= lon[
                    j] and tly[i] >= lat[j] and trx[i] >= lon[j] and tpry[i] >= lat[j]:
                    list=[5,high[j],medium[j],low[j],0.5]
                    wcount = wcount + WeightCalculate.calculate(list)
                else:
                    pass
            weight.append(wcount)
        print(weight)
        try:
            x = min(weight)
            mxrs = weight.index(x)
        except:
            mxrs = -1
        context = {
            "blx": blx, "bly": bly, "brx": brx, "bry": bry, "tlx": tlx, "tly": tly, "trx": trx, "tpry": tpry,
            "lat": lat, "lon": lon, "mxrs_index": mxrs
        }

        return render(request, 'visual.html', context)


class readData(View):
    template = "read.html"

    def get(self, request):
        return render(request, self.template)


class coordinate(View):
    template = "coordinate.html"

    def post(self, request):
        type = request.POST.get('type')
        fs = request.FILES['csv']
        data = fs.read().decode("ANSI")
        lines = data.split("\n")
        x = []
        for line in lines:
            fields = line.split(",")
            x.append(fields)
        inst = []
        for line in x:
            if len(line) > 1:
                if type == line[3]:
                    if line[17] == '\r' or line[17] == '' or line[16] == 'Longitude':
                        pass
                    else:
                        inst.append(line[5].replace(" ", "+"))
        context = {
            "institute": inst
        }
        return render(request, self.template, context)

class AddObject(View):
    template = "import.html"
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('maxrs:login')
        return render(request, self.template)

    def post(self, request):
        fs = request.FILES['csv']
        data = fs.read().decode("ANSI")
        lines = data.split("\n")
        x = []
        lat = []
        lon = []
        high, medium, low = [], [], []
        for line in lines:
            fields = line.split(",")
            x.append(fields)
        for line in x:
            print(line)
            if line[13] == '':
                male=0
            else:
                male=int(line[13])
            if line[14] == '':
                female = 0
            else:
                female = int(line[14])
            if line[15] == '':
                ttl = 0
            else:
                ttl = int(line[15])

            data=SpatialData.objects.create(district=line[0],thana=line[1],type=line[2],level=line[3],code=line[4],name=line[5],address=line[6],post_office=line[7],mobile=line[8],management=line[9],gender_type=line[10],area_type=line[11],geography=line[12],no_of_male=male,no_of_female=female,total=ttl,longitude=line[16],lattitude=line[17],high_income=float(line[18]),mid_income=float(line[19]),low_income=float(line[20]))
            data.save()
            messages.success(request, 'Objects are imported successfully')
        return redirect("maxrs:add_object")

class getForm(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('maxrs:login')
        form=CreateObject()
        return render(request, 'form.html', {"form":form})
    def post(self, request):
        form=CreateObject(request.POST)
        if form.is_valid():
            form.save()
            return redirect('maxrs:add_object')
        else:
            return redirect('maxrs:add_object')


class getSignUp(View):
    def get(self, request):
        return render(request, 'register.html',{"form":SignUpForm})
    def post(self, request):
        form=SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('maxrs:login')
        else:
            return redirect('maxrs:register')



class getLogin(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('maxrs:home')
        return render(request, 'login.html')
    def post(self, request):
        username=request.POST.get('username')
        password=request.POST.get('password')
        auth=authenticate(request, username=username, password=password)
        if auth is not None:
            login(request, auth)
            return redirect('maxrs:home')
        else:
            return redirect('maxrs:login')

class LogOut(View):
    def get(self, request):
        logout(request)
        return redirect('maxrs:home')


class JunctionAdd(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('maxrs:login')
        return render(request, 'junctionadd.html', {"form":CreateJunction})
    def post(self, request):
        form=CreateJunction(request.POST)
        if form.is_valid():
            form.save()
            return redirect('maxrs:JunctionAdd')
        else:
            return redirect('maxrs:JunctionAdd')

class JunctionImport(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('maxrs:login')
        return render(request, 'import.html')
    def post(self, request):
        fs = request.FILES['csv']
        data = fs.read().decode("ANSI")
        lines = data.split("\n")
        x = []
        for line in lines:
            fields = line.split(",")
            x.append(fields)
        print(x)
        for line in x:
            data = Junction.objects.create(name=line[1], longitude=line[3], lattitude=line[2])
            data.save()
        return HttpResponse("Data is saved successfully")



class AreaImport(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('maxrs:login')
        return render(request, 'import.html')
    def post(self, request):
        fs = request.FILES['csv']
        data = fs.read().decode("utf-8")
        lines = data.split("\n")
        x = []
        for line in lines:
            fields = line.split(",")
            x.append(fields)
        print(x)
        for line in x:
            data = AreaInfo.objects.create(thana=str(line[1]), population=int(line[2]), area_size=float(line[3]))
            data.save()
        return HttpResponse("Data is saved successfully")







'''Documentation part'''
class getDocs(ListView):
    model = Documentation



class Document(DetailView):
    model = Documentation
    slug_url_kwarg = 'slug'
    query_pk_and_slug = True