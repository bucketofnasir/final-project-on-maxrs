class rect:
    def TriangleArea(x1, y1, x2, y2, x3, y3):
        return abs((x1 * (y1 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0)

    '''A function to check whether point P(x, y) lies
    inside the rectangle formed by A(x1, y1),B(x2, y2),
     C(x3, y3) and D(x4, y4)'''

    def check(x1, y1, x2, y2, x3, y3, x4, y4, x, y):
        # Calculate area of rectangle ABCD
        A = rect.TriangleArea(x1, y1, x2, y2, x3, y3) + rect.TriangleArea(x1, y1, x4, y4, x3, y3)
        # Calculate area of triangle PAB
        A1 = rect.TriangleArea(x, y, x1, y1, x2, y2)
        # Calculate area of triangle PBC
        A2 = rect.TriangleArea(x, y, x2, y2, x3, y3)
        # Calculate area of triangle PCD
        A3 = rect.TriangleArea(x, y, x3, y3, x4, y4)
        # Calculate area of triangle PAD
        A4 = rect.TriangleArea(x, y, x1, y1, x4, y4)

        # Check if sum of A1, A2, A3 and A4
        addition = A1 + A2 + A3 + A4
        if A == addition:
            return True
        else:
            return False

    def is_in_rectangle(x1, y1, x2, y2, x3, y3, x4, y4, x, y):
        if x >= x1 and y >= y1 and x <= x2 and y >= y2 and x <= x3 and y <= y3 and x >= x4 and y <= y4:
            return True
        return False
