from django.urls import path
from . import views

app_name = "projectApp"
urlpatterns = [
    path('', views.getHome.as_view(), name="home"),
    path('visual', views.getIndex.as_view(), name="visual"),
    path('visual/form', views.VisualizeForm.as_view(), name="visual_from_db"),
    path('visual/view', views.VisualizeData.as_view(), name="visual_view_db"),
    path('login', views.getLogin.as_view(), name="login"),
    path('register', views.getSignUp.as_view(), name="register"),
    path('coordinate', views.coordinate.as_view(), name="coordinate"),
    path('import/object',views.AddObject.as_view(), name="add_object"),
    path('create/object',views.getForm.as_view(), name="create"),
    path('create/junction',views.JunctionAdd.as_view(), name="JunctionAdd"),
    path('import/junction', views.JunctionImport.as_view(), name="JunctionImport"),
    path('import/area', views.AreaImport.as_view(), name="AreaImport"),
    path('logout',views.LogOut.as_view(), name="logout"),

    path('docs', views.getDocs.as_view(template_name='documentation.html'), name="Documents"),
    path('doc/<slug>', views.Document.as_view(template_name='single_doc.html'), name="SingleDoc"),
]