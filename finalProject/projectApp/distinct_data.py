from .models import SpatialData
class data():
    def distinct(self):
        geography, area_type, district, level, management, gender_type, thana = [], [], [], [], [], [], []
        data = SpatialData.objects.all()
        for d in data:
            if d.geography not in geography:
                geography.append(d.geography)
            if d.district not in district:
                district.append(d.district)
            if d.area_type not in area_type:
                area_type.append(d.area_type)
            if d.level not in level:
                level.append(d.level)
            if d.management not in management:
                management.append(d.management)
            if d.gender_type not in gender_type:
                gender_type.append(d.gender_type)
            if d.thana not in thana:
                thana.append(d.thana)
        map_type=["topo","topo-vector","satellite","hybrid","terrain"]

        return {
            "geography": geography,
            "district": district,
            "area_type": area_type,
            "level": level,
            "management": management,
            "gender_type": gender_type,
            "thana": thana,
            "map_tp":map_type
        }