from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db.models.signals import pre_save
from django.utils.text import slugify
# Create your models here.
class SpatialData(models.Model):
    district=models.CharField(max_length=100)
    thana=models.CharField(max_length=100)
    post_office=models.CharField(max_length=100)
    type=models.CharField(max_length=100)
    level=models.CharField(max_length=100)
    code=models.CharField(max_length=100)
    name=models.CharField(max_length=100)
    address=models.TextField()
    mobile=models.CharField(max_length=100)
    management=models.CharField(max_length=100)
    gender_type=models.CharField(max_length=100)
    area_type=models.CharField(max_length=100)
    geography=models.CharField(max_length=100)
    no_of_male=models.IntegerField(default=0, null=True, blank=True)
    no_of_female=models.IntegerField(default=0,null=True, blank=True)
    total=models.IntegerField(default=0, null=True, blank=True)
    longitude=models.CharField(max_length=50)
    lattitude=models.CharField(max_length=50)
    high_income=models.DecimalField(max_digits=10, decimal_places=5)
    mid_income=models.DecimalField(max_digits=10, decimal_places=5)
    low_income=models.DecimalField(max_digits=10, decimal_places=5)
    def __str__(self):
        return self.name.title()

class Junction(models.Model):
    name=models.CharField(max_length=200)
    longitude = models.CharField(max_length=12)
    lattitude = models.CharField(max_length=12)

    def __str__(self):
        return self.name

class AreaInfo(models.Model):
    thana = models.CharField(max_length=200)
    population = models.IntegerField()
    area_size=models.DecimalField(max_digits=5, decimal_places=2)
    def __str__(self):
        return self.thana

class Documentation(models.Model):
    title=models.CharField(max_length=200)
    body=RichTextField()
    slug=models.SlugField(unique=True, blank=True, null=True)
    def __str__(self):
        return self.title.title()

def create__slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    s = Documentation.objects.filter(slug=slug).order_by("-id")
    exist = s.exists()
    if exist:
        new_slug = "%s-%s" % (slug, s.first().id)
        return create__slug(instance, new_slug=new_slug)
    return slug


def pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create__slug(instance)


pre_save.connect(pre_save_receiver, sender=Documentation)