from django.contrib import admin
from .models import SpatialData, Junction, AreaInfo, Documentation
from django_admin_listfilter_dropdown.filters import (DropdownFilter, ChoiceDropdownFilter, RelatedDropdownFilter)
# Register your models here.


class SpatialDataModel(admin.ModelAdmin):
    list_display = ["__str__", "address", "mobile"]
    list_filter = ["level",
                   "type",
                   "management",
                   "gender_type",
                   "area_type",
                   "geography",
                   ("thana", DropdownFilter)
                   ]
    fieldsets = [('Location Info', {'fields': ['district', 'thana','post_office','code','address']}),
                 ("Basic Info", {'fields': ['name', 'type', 'level', 'management', 'gender_type','area_type']}),
                 ("Geographical Info", {'fields': ['geography','longitude', 'lattitude']}),
                 ("Income family info", {'fields': ['high_income', 'mid_income', 'low_income']}),
                 ("Contact Info", {'fields': ['mobile']}),
                 ("Others Info", {'fields': ['no_of_male','no_of_female','total']}),
                 ]
    list_per_page = 50
    search_fields = ["__str__", "district", "thana", "code", "address", "mobile", "longitude"]

    class Meta:
        model = SpatialData


admin.site.register(SpatialData, SpatialDataModel)

class JunctionModel(admin.ModelAdmin):
    list_display = ["__str__", "longitude", "lattitude"]
    list_per_page = 20
    search_fields = ["__str__", "longitude", "lattitude"]

    class Meta:
        model = Junction
admin.site.register(Junction, JunctionModel)

class AreaInfoModel(admin.ModelAdmin):
    list_display = ["__str__", "population", "area_size"]
    list_per_page = 20
    search_fields = ["__str__"]

    class Meta:
        model = AreaInfo
admin.site.register(AreaInfo, AreaInfoModel)

class DocModel(admin.ModelAdmin):
    list_display = ["__str__"]
    list_per_page = 20
    search_fields = ["__str__"]

    class Meta:
        model = Documentation
admin.site.register(Documentation, DocModel)
